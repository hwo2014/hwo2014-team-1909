#!/usr/bin/env python
import os
import socket
import sys

from roypec.dataset import DataSet, TelemetryRecorder
from roypec.adapter import DataSetSyncAdapter, SynchronousSocketAdapter
from roypec.game import Game
from roypec.brain import MouseBrain
from roypec.physics import BreakAngleModel, ThrottleModel, DriftModel
from roypec.handler import GameHandler

def main(host, port, name, key):
    game = Game()
    critical_angle_model = BreakAngleModel(game)
    throttle_model = ThrottleModel(game)
    drift_model = DriftModel(game)
    brain = MouseBrain(game, critical_angle_model, throttle_model,
                       drift_model)
    observers = [critical_angle_model, throttle_model, drift_model]
    handler = GameHandler(game, brain, observers)

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, int(port)))
    adapter = SynchronousSocketAdapter(handler, sock, None)
    adapter.send_join_request(name, key)
    adapter.run()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        main(*sys.argv[1:])
