import const
import numpy as np


# Notation: `d` stands for in-piece-distance and `o` for the offset relative
# to piece midpoint across the track. Occasionally `u` stands for some other
# variable mapped to unit interval.


def straight_switch_profile(piece_length, in_offset, out_offset, n_samples=100):
    delta_offset = float(out_offset - in_offset)
    x_piece = np.linspace(0.0, piece_length, n_samples)
    u = np.linspace(0.0, piece_length, n_samples)  / float(piece_length)
    offset = in_offset + \
        delta_offset * (0.667 * u + 0.1665 * (1.0 - np.cos(np.pi * u)))
    return x_piece, offset


def calc_profile_length(x_piece, y_piece):
    dx_piece = np.diff(x_piece)
    dy_piece = np.diff(y_piece)
    step_lengths = np.sqrt(dx_piece * dx_piece + dy_piece * dy_piece)
    return sum(step_lengths)


def curved_switch_profile(curve_angle, radius, in_offset, out_offset, n_samples=100):
    phis = np.linspace(0, np.pi * curve_angle / 180.0, n_samples)
    if radius > 0.0:
        rs = radius - np.linspace(in_offset, out_offset, n_samples)
        xs = rs * np.sin(phis)
        ys = radius - rs * np.cos(phis)
    else:
        rs = radius + np.linspace(in_offset, out_offset, n_samples)
        xs = rs * np.sin(phis)
        ys = rs * np.cos(phis) - radius
    return xs, ys


def calc_curvature(xs, ys, in_angle=0.0, out_angle=0.0):
    d_xs = np.diff(xs, 1)
    d_ys = np.diff(ys, 1)
    ns = np.sqrt(d_xs * d_xs + d_ys * d_ys)
    # (Unit) direction vectors
    u_xs = d_xs / ns
    u_ys = d_ys / ns
    # Right-hand normals
    n_xs = -u_ys
    n_ys = u_xs
    # Curvature
    crv = np.zeros(xs.shape)
    crv[1:-1] = u_xs[1:] * n_xs[:-1] + u_ys[1:] * n_ys[:-1]
    # Inward end
    n_x_in = np.cos(np.pi * (in_angle + 90.0) / 180.0)
    n_y_in = np.sin(np.pi * (in_angle + 90.0) / 180.0)
    crv[0] = u_xs[0] * n_x_in + u_ys[0] * n_y_in
    # Outward end
    d_x_out = np.cos(np.pi * in_angle / 180.0)
    d_y_out = np.sin(np.pi * in_angle / 180.0)
    crv[-1] = d_x_out * n_xs[-1] + d_y_out * n_ys[-1]
    # In-piece-distance
    dist = np.zeros(crv.shape)
    dist[1:] = np.cumsum(ns)
    return dist, crv


class Segment(object):

    @classmethod
    def straight_segment(cls, length):
        return cls(length, 0.0)

    @classmethod
    def curved_segment(cls, angle, radius, offset):
        side = angle / abs(angle)
        radius -= side * offset
        curvature = side / radius
        length = 2.0 * np.pi * radius * abs(angle) / 360.0
        return cls(length, curvature)

    @classmethod
    def straight_switch_segment(cls, straight_length, in_offset, out_offset):
        x_piece, y_piece = straight_switch_profile(straight_length, in_offset,
                                                   out_offset)
        length = calc_profile_length(x_piece, y_piece)
        # FIXME: Curvature (but it is mostly almost straight anyway)
        return cls(length, 0.0)

    @classmethod
    def curved_switch_segment(cls, angle, radius, in_offset, out_offset):
        x_piece, y_piece = curved_switch_profile(angle, radius, in_offset, out_offset)
        length = calc_profile_length(x_piece, y_piece)
        side = angle / abs(angle)
        in_radius = radius - side * in_offset
        in_curvature = side / in_radius
        out_radius = radius - side * out_offset
        out_curvature = side / out_radius
        return cls(length, in_curvature, out_curvature)

    def __init__(self, length, in_curvature, out_curvature=None):
        # Inherent props
        self.length = length
        self.in_curvature = in_curvature
        self.out_curvature = out_curvature
        # Make sense only in context of track
        self.ix = None
        self.piece_ix = None
        self.left = None
        self.right = None
        self.straight = None

    def curvature_at(self, segment_dist):
        if self.out_curvature is None:
            return self.in_curvature
        else:
            a = 1.0 / self.in_curvature
            b = 1.0 / self.out_curvature
            alpha = segment_dist / self.length
            c = a + (b - a) * alpha
            return 1.0 / c

    @property
    def direction(self):
        if self.curvature == 0.0:
            return const.STRAIGHT
        elif self.curvature < 0.0:
            return const.LEFT
        else:
            return const.RIGHT

    @property
    def is_branch_point(self):
        return (self.left or self.right) is not None

    def list_options(self):
        res = [(const.STRAIGHT, self.straight)]
        if self.left:
            res.append((const.LEFT, self.left))
        if self.right:
            res.append((const.RIGHT, self.right))
        return res

    def iter_till_branch_point(self):
        segment = self
        while True:
            yield segment
            if segment.is_branch_point:
                break
            segment = segment.straight
            if segment == self:
                break

    def iter_according_to_plan(self, plan=[]):
        plan = plan[:]
        segment = self
        while True:
            yield segment
            if segment.is_branch_point:
                if plan:
                    direction = plan.pop(0)
                    if direction == const.LEFT and segment.left:
                        segment = segment.left
                    elif direction == const.RIGHT and segment.right:
                        segment = segment.right
                    elif direction == const.STRAIGHT:
                        segment = segment.straight
                    else:
                        segment = segment.straight
                        print 'WARNING: Plan inconsistent with track layout'
                else:
                    segment = segment.straight
            else:
                segment = segment.straight
            if segment.piece_ix == self.piece_ix:
                break


class Lane(object):

    def __init__(self, ix, offset):
        self.ix = ix
        self.offset = offset
        self.left = None
        self.right = None


def build_lanes(api_data):
    left_to_right = []
    for lane_data in api_data:
        ix = int(lane_data['index'])
        offset = float(lane_data['distanceFromCenter'])
        left_to_right.append((offset, ix))
    left_to_right.sort()
    lanes = {}
    for offset, ix in left_to_right:
        lanes[ix] = Lane(ix, offset)
    for (_, left_ix), (_, right_ix) in zip(left_to_right[:-1],
                                           left_to_right[1:]):
        lanes[left_ix].right = lanes[right_ix]
        lanes[right_ix].left = lanes[left_ix]
    return lanes


def build_segment(api_data, in_lane, out_lane=None):
    if out_lane is None:
        if 'angle' in api_data:
            return Segment.curved_segment(float(api_data['angle']),
                                          float(api_data['radius']),
                                          in_lane.offset)
        else:
            return Segment.straight_segment(float(api_data['length']))
    else:
        if 'angle' in api_data:
            return Segment.curved_switch_segment(float(api_data['angle']),
                                                 float(api_data['radius']),
                                                 in_lane.offset,
                                                 out_lane.offset)
        else:
            return Segment.straight_switch_segment(float(api_data['length']),
                                                   in_lane.offset,
                                                   out_lane.offset)


def build_all_segments(api_data, lanes):
    segments = {}
    connection_graph = {}
    piece_count = len(api_data)
    for piece_ix, piece_data in enumerate(api_data):
        for lane in lanes.itervalues():
            segments[(piece_ix, lane.ix, lane.ix)] = segment \
                = build_segment(piece_data, lane)
            connection_graph[(piece_ix, lane.ix)] = connections \
                = {'straight': segment}
            if piece_data.get('switch', False):
                if lane.left:
                    segments[(piece_ix, lane.ix, lane.left.ix)] = segment \
                        = build_segment(piece_data, lane, lane.left)
                    connections['left'] = segment
                if lane.right:
                    segments[(piece_ix, lane.ix, lane.right.ix)] = segment \
                        = build_segment(piece_data, lane, lane.right)
                    connections['right'] = segment
    for segment_ix, segment in segments.iteritems():
        piece_ix, _, lane_ix = segment_ix
        next_piece_ix = (piece_ix + 1) % piece_count
        connections = connection_graph[(next_piece_ix, lane_ix)]
        segment.ix = segment_ix
        segment.piece_ix = piece_ix
        segment.straight = connections['straight']
        segment.left = connections.get('left')
        segment.right = connections.get('right')
    return segments


class Track(object):

    @classmethod
    def from_api_data(cls, api_data):
        lanes = build_lanes(api_data['lanes'])
        segments = build_all_segments(api_data['pieces'], lanes)
        piece_count = len(api_data['pieces'])
        return cls(lanes, segments, piece_count)

    def __init__(self, lanes, segments, piece_count):
        self.lanes = lanes
        self.segments = segments
        self.piece_count = piece_count

    def calc_dist(self, seg_ix_1, seg_dist_1, seg_ix_2, seg_dist_2):
        if seg_ix_1 == seg_ix_2:
            return seg_dist_2 - seg_dist_1
        else:
            # NOTE: A reckless assumption of not being able to zoom through a
            # whole segment in one tick.
            return self.segments[seg_ix_1].length - seg_dist_1 + seg_dist_2

    def curvature_at(self, segment_ix, segment_dist, step_length):
        # FIXME: Currently step length ignored.
        return self.segments[segment_ix].curvature_at(segment_dist)

    def following_segments(self, segment_ix):
        piece_ix, _, out_lane = segment_ix
        res = []
        next_piece_ix = (piece_ix + 1) % self.piece_count
        for cand_segment_ix in self.segments.iterkeys():
            if cand_segment_ix[0] == next_piece_ix and \
               cand_segment_ix[1] == out_lane:
                res.append(cand_segment_ix)
        return res

    def iter_segments_from(self, segment_ix, n=None):
        segment = self.segments[segment_ix]
        while n is None or n > 0:
            yield segment
            segment = segment.straight
            if n is not None:
                n -= 1

    def curvatures_from(self, segment_ix, segment_dist, distances, plan=[]):
        curvatures = np.zeros_like(distances)
        segment_iter = self.segments[segment_ix].iter_according_to_plan(plan)
        segment = segment_iter.next()
        for i, d in enumerate(np.diff(distances, 1)):
            curvatures[i] = segment.curvature_at(segment_dist)
            segment_dist += d
            while segment_dist > segment.length:
                segment_dist -= segment.length
                segment = segment_iter.next()
        return curvatures

    def segments_of_piece(self, piece_ix):
        for (piece_ix, in_lane, out_lane), segment in self.segments.iteritems():
            yield (in_lane, out_lane, segment)

    def directions_for(self, segment_ix):
        return self.segments[segment_ix].direction

    def is_vanilla(self, segment_ix):
        _, in_lane, out_lane = segment_ix
        return in_lane == out_lane
