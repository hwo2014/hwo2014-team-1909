import json
import os
import Queue as queue
import re
import time
import traceback


class Adapter(object):

    def __init__(self, handler, running_condition):
        self.handler = handler
        self.running_condition = running_condition

    def run(self):
        while self.running_conditiong.can_run:
            pass


class SynchronousSocketAdapter(object):

    def __init__(self, handler, socket, recorder=None):
        self.handler = handler
        self.outbox = queue.Queue()
        self.socket = socket
        self.socket_in_file = socket.makefile()
        self.recorder = recorder

    def _send(self, message):
        json_str = json.dumps(message,separators=(',', ':'))
        self.socket.sendall(json_str + '\n')
        if self.recorder:
            self.recorder.record('SEND', json_str)

    def _recv(self):
        line = self.socket_in_file.readline()
        while line:
            json_str = line.rstrip()
            if self.recorder:
                self.recorder.record('RECV', json_str)
            try:
                return json.loads(json_str)
            except:
                traceback.print_exc()
            line = self.in_file.readline()

    def send_join_request(self, name, key, track_name=None, car_count=None,
                          password=None):
        simple = True
        data = {
            'botId': {
                'name': name,
                'key': key
            },
        }
        if track_name is not None:
            data['trackName'] = track_name
            simple = False
        if car_count is not None:
            data['carCount'] = car_count
            simple = False
        if password is not None:
            data['password'] = password
            simple = False
        if simple:
            out_msg = {'msgType': 'join',
                       'data': data['botId']}
        else:
            out_msg = {'msgType': 'joinRace',
                       'data': data}
        self._send(out_msg)

    def run(self):
        in_message = self._recv()
        while in_message:
            self.handler.handle(in_message, lambda x: self.outbox.put(x))
            while True:
                try:
                    out_message = self.outbox.get_nowait()
                except queue.Empty:
                    break
                self._send(out_message)
                self.outbox.task_done()
            in_message = self._recv()


class DataSetSyncAdapter(object):

    RECV_PAT = re.compile(r'^RECV\s+(\d+\.\d+)\s+([^ ].*)$', re.DOTALL)

    def __init__(self, handler, data_set, recorder=None):
        self.handler = handler
        self.in_file = data_set.open_file('messages.log')
        self.outbox = queue.Queue()
        self.recorder = recorder

    def _send(self, message):
        json_str = json.dumps(message,separators=(',', ':'))
        if self.recorder:
            self.recorder.record('SEND', json_str)

    def _recv(self):
        line = self.in_file.readline()
        while line:
            line = line.rstrip()
            match = self.RECV_PAT.match(line)
            if match:
                json_str = match.group(2).rstrip()
                if self.recorder:
                    self.recorder.set_time(float(match.group(1)))
                    self.recorder.record('RECV', json_str)
                try:
                    return json.loads(json_str)
                except:
                    pass
            line = self.in_file.readline()

    def run(self):
        in_message = self._recv()
        while in_message:
            self.handler.handle(in_message, lambda x: self.outbox.put(x))
            while True:
                try:
                    out_message = self.outbox.get_nowait()
                except queue.Empty:
                    break
                self._send(out_message)
                self.outbox.task_done()
            in_message = self._recv()
