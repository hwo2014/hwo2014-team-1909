import numpy as np

from roypec.track import Track
from roypec.util import method_not_implemented


class Car(object):

    class State(object):

        def __init__(self):
            self.reset()

        def reset(self):
            #
            self.advanced_to = -1
            self.crashed_on = -2
            self.spawned_on = 0
            self.turbo_available = False
            self.turbo_started_on = -1
            self.turbo_expired_on = 0
            self.turbo_factor = 0.0
            self.turbo_remaining = 0
            self.pending_lane_switch = 0
            #
            self.reset_physics()
            # Lagged values (will not be reset when we reset physics so that
            # we can inspect them just after a crash)
            self.prev_vanilla = None
            self.prev_angle = None
            self.prev_curvature = None
            self.prev_delta_angle = None
            self.prev_in_segment_dist = None
            self.prev_segment_ix = None
            self.prev_throttle = None
            self.prev_velocity = None

        def reset_physics(self):
            self.ticks_since_physics_reset = 0
            # Current values
            self.vanilla = None
            self.angle = None
            self.curvature = None
            self.in_segment_dist = None
            self.segment_ix = None
            self.throttle = None
            # Current derived values
            self.delta_angle = None
            self.velocity = None

        def advance(self, tick):
            self.prev_vanilla = self.vanilla
            self.prev_angle = self.angle
            self.prev_curvature = self.curvature
            self.prev_delta_angle = self.delta_angle
            self.prev_in_segment_dist = self.in_segment_dist
            self.prev_segment_ix = self.segment_ix
            self.prev_throttle = self.throttle
            self.prev_velocity = self.velocity
            self.ticks_since_physics_reset += 1
            self.advanced_to = tick

    @classmethod
    def from_desc(cls, api_data, track=None):
        d = api_data['dimensions']
        i = api_data['id']
        guide_pos = float(d['guideFlagPosition'])
        width = float(d['width'])
        length = float(d['length'])
        name = i['name']
        color = i['color']
        return cls(guide_pos, width, length, color=color, name=name,
                   track=track)

    def __init__(self, guide_pos, width, length, color=None, name=None, track=None):
        self.guide_pos = guide_pos
        self.width = width
        self.length = length
        self.color = color
        self.name = name
        self.track = track
        self.state = self.State()

    @property
    def is_active(self):
        s = self.state
        return s.crashed_on < s.spawned_on

    @property
    def is_turbo_active(self):
        s = self.state
        return s.turbo_started_on > s.turbo_expired_on

    @property
    def boost_factor(self):
        if self.is_turbo_active:
            return self.state.turbo_factor
        else:
            return 1.0

    @property
    def did_just_crash(self):
        s = self.state
        return s.crashed_on > 0 and s.crashed_on == s.advanced_to

    @property
    def crashed_on_previous_tick(self):
        s = self.state
        return s.crashed_on > 0 and s.crashed_on == s.advanced_to - 1

    def advance_state(self, tick):
        self.state.advance(tick)

    def crash(self, tick):
        self.state.crashed_on = tick
        self.state.reset_physics()

    def spawn(self, tick):
        self.state.spawned_on = tick

    def charge_turbo(self, factor, duration):
        self.state.turbo_available = True
        self.state.turbo_factor = factor
        # FIXME: Change to ticks instead of msecs
        self.state.turbo_remaining = duration

    def start_turbo(self, tick):
        self.state.turbo_available = False
        self.turbo_started_on = tick

    def end_turbo(self, tick):
        self.turbo_expired_on = tick

    def reset(self):
        self.state.reset()

    def set_throttle(self, throttle):
        self.state.throttle = throttle
        if self.state.prev_throttle is None:
            print 'INFO: set throttle to {:.3f}'.format(throttle)
        elif np.abs(throttle - self.state.prev_throttle) > 0.05:
            print 'INFO: adjust throttle from {:.3f} to {:.3f}' \
                  .format(self.state.prev_throttle, throttle)

    def set_pending_switch(self, side):
        self.state.pending_lane_switch = side

    def move_to(self, tick, segment_ix, in_segment_dist, angle):
        s = self.state
        # Direct state values
        s.segment_ix = segment_ix
        s.in_segment_dist = in_segment_dist
        s.angle = angle
        # Derived values
        if s.ticks_since_physics_reset > 1:
            # d(Angle)
            s.delta_angle = s.angle - s.prev_angle
            # Velocity
            tick_dist = \
                self.track.calc_dist(s.prev_segment_ix, s.prev_in_segment_dist,
                                     s.segment_ix, s.in_segment_dist)
            s.velocity = 60.0 * tick_dist
            s.curvature = \
                self.track.curvature_at(s.segment_ix, s.in_segment_dist,
                                        tick_dist)
            s.vanilla = \
                self.track.is_vanilla(s.segment_ix)

    note_position = move_to

    @property
    def has_complete_data(self):
        return self.state.ticks_since_physics_reset > 2


class Game(object):

    def __init__(self):
        self.cars = {}
        self.my_color = None
        self.my_car = None
        self.track = None
        self.is_active = False

    def iter_opponents(self):
        for car in self.cars.itervalues():
            if car is not self.my_car:
                yield car

    def init_from_desc(self, api_data):
        r = api_data['race']
        self.track = track = Track.from_api_data(r['track'])
        for car_api_data in r['cars']:
            car = Car.from_desc(car_api_data, track=track)
            self.cars[car.color] = car
        if self.my_color is not None:
            self.my_car = self.cars[self.my_color]
        print 'INFO: game object initialized'
        return self

    def advance_state(self, tick):
        for car in self.cars.itervalues():
            car.advance_state(tick)

    def note_crash(self, tick, car_id):
        self.cars[car_id].crash(tick)
        if car_id == self.my_color:
            print 'INFO: crashed'

    def note_lap_finished(self, tick, car_id, lap):
        method_not_implemented(self)

    def note_spawn(self, tick, car_id):
        self.cars[car_id].spawn(tick)
        if car_id == self.my_color:
            print 'INFO: spawned'

    def note_turbo_availability(self, factor, duration):
        for car in self.cars.itervalues():
            car.charge_turbo(factor, duration)

    def note_turbo_start(self, tick, car_id):
        self.cars[car_id].start_turbo(tick)

    def note_turbo_end(self, tick, car_id):
        self.cars[car_id].end_turbo(tick)

    def start(self, tick):
        self.is_active = True

    def end(self, tick):
        self.is_active = False

    def end_tournament(self):
        method_not_implemented(self)

    def set_own_car(self, car_id):
        self.my_color = car_id
