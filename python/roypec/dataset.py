import datetime
import os
import re
import sys
import time


def get_utc_timestamp():
    return datetime.datetime.utcnow().strftime('%Y%m%dT%H%M%S')


class TelemetryRecorder(object):

    @classmethod
    def get_stdout_recorder(cls):
        return cls(sys.stdout)

    def __init__(self, out_file):
        self.out_file = out_file
        self.set_time(0.0)

    def set_time(self, offset):
        self.offset = offset
        self.offset_set_on = time.time()

    def record(self, tag, line):
        elapsed = (time.time() - self.offset_set_on) * 1000.0
        timestamp = self.offset + elapsed
        print >> self.out_file, '{:s} {:.2f} {:s}'.format(tag, timestamp, line)


telemetry_pat = re.compile(r'(RECV|SEND)\s+(\d+\.\d+)\s+(.*)$')


class TelemetryReader(object):

    class Iter(object):

        def __init__(self, r):
            self.r = r

        def __iter__(self):
            return self

        def next(self):
            if self.r.f is None:
                raise IOError('reader closed')
            l = self.r.f.readline()
            if not l:
                raise StopIteration
            m = telemetry_pat.match(l)
            if m is None:
                raise RuntimeError('cannot parse: "{}"'.format(l.rstrip()))
            tag = m.group(1)
            ts = float(m.group(2))
            msg = m.group(3).strip()
            return (tag, ts, msg)

    def __init__(self, f):
        self.f = f
        self.i = self.Iter(self)

    def __iter__(self):
        return self.i

    def __enter__(self):
        return self.i

    def __exit__(self, _1, _2, _3):
        self.f.close()
        self.f = None


class DataSet(object):

    @classmethod
    def get_latest(cls, suffix=None):
        data_dir = os.environ['HWO_DATA_DIR']
        if suffix:
            pattern = re.compile(r'\d{8}T\d{6}-' + suffix + r'\.dat')
        else:
            pattern = re.compile(r'\d{8}T\d{6}\.dat')
        for file_name in sorted(os.listdir(data_dir), reverse=True):
            if pattern.match(file_name):
                path = os.path.join(data_dir, file_name)
                return cls(path)

    def __init__(self, name=None, suffix=None):
        if name:
            self.name = name
        elif suffix is not None:
            self.name = '{timestamp:s}-{suffix:s}.dat' \
                .format(timestamp=get_utc_timestamp(), suffix=suffix)
        else:
            self.name = '{timestamp:s}.dat' \
                .format(timestamp=get_utc_timestamp())
        data_dir = os.environ['HWO_DATA_DIR']
        if not os.path.isdir(data_dir):
            raise RuntimeError('data directory "{0}" does not exist or is '
                               'not a directory'.format(data_dir))
        self.path = os.path.join(data_dir, self.name)
        if name and not os.path.isdir(self.path):
            raise RuntimeError('data set "{}" does not exist in the data '
                               'directory "{}"'.format(name, data_dir))

    def open_file(self, name, mode='r'):
        if 'r' not in mode and not os.path.exists(self.path):
            os.makedirs(self.path)
        return open(os.path.join(self.path, name), mode)

    def open_telemetry(self):
        f = self.open_file('messages.log', 'r')
        return TelemetryReader(f)

    def get_recorder(self):
        f = self.open_file('messages.log', 'w')
        return TelemetryRecorder(f)
