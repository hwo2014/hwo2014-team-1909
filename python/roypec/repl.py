import os
import socket

from roypec.adapter import DataSetSyncAdapter, SynchronousSocketAdapter
from roypec.brain import MouseBrain
from roypec.dataset import DataSet, TelemetryRecorder
from roypec.game import Game
from roypec.handler import GameHandler
from roypec.physics import BreakAngleModel, ThrottleModel, DriftModel


def replay_latest(suffix=None):
    in_data_set = DataSet.get_latest(suffix)
    if in_data_set:
        game = Game()
        critical_angle_model = BreakAngleModel(game)
        throttle_model = ThrottleModel(game)
        drift_model = DriftModel(game)
        brain = MouseBrain(game, critical_angle_model, throttle_model,
                           drift_model)
        observers = [critical_angle_model, throttle_model, drift_model]
        handler = GameHandler(game, brain, observers)
        out_data_set = DataSet(suffix='replay')
        recorder = out_data_set.get_recorder()
        adapter = DataSetSyncAdapter(handler, in_data_set, recorder=recorder)
        print 'Recording will go into data set {:s}'.format(out_data_set.name)
        adapter.run()


def race(host=None, port=None, name=None, key=None, track_name='keimola',
         car_count=1, password=None):

    host = host or os.environ['HWO_TEST_HOST']
    port = port or int(os.environ['HWO_TEST_PORT'])
    name = name or os.environ['HWO_BOT_NAME']
    key = key or os.environ['HWO_BOT_KEY']

    game = Game()
    critical_angle_model = BreakAngleModel(game)
    throttle_model = ThrottleModel(game)
    drift_model = DriftModel(game)
    brain = MouseBrain(game, critical_angle_model, throttle_model,
                       drift_model)
    observers = [critical_angle_model, throttle_model, drift_model]
    handler = GameHandler(game, brain, observers)
    out_data_set = DataSet()
    recorder = out_data_set.get_recorder()

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    adapter = SynchronousSocketAdapter(handler, sock, recorder)

    print 'Recording will go into data set {:s}'.format(out_data_set.name)
    adapter.send_join_request(name, key, track_name, car_count, password)
    adapter.run()
