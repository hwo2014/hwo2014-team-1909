import numpy as np
import numpy.linalg as la


# FIXME: There are no bounds checks on updates to backing array. They are
# sized for 20 laps, 1200 ticks each with 6 cars racing; that should be
# enough. But it is ugly anyway.
MAX_OBSERVATIONS = 20 * 1200 * 6


class BreakAngleModel(object):

    def __init__(self, game):
        self.game = game
        self.angle = 60.0
        self.should_fit = False

    def collect_data(self):
        car = self.game.my_car
        s = car.state
        if car.did_just_crash:
            if abs(s.prev_angle) < self.angle:
                self.angle = abs(s.prev_angle)
                print 'INFO: safe angle decreased to {:f}' \
                      .format(self.angle)
        elif car.is_active:
            if abs(s.angle) > self.angle:
                self.angle = abs(s.angle)
                print 'INFO: safe angle incresed to {:f}' \
                      .format(self.angle)


class ThrottleModel(object):

    def __init__(self, game, beta_velocity=0.98, beta_throttle=12.0):
        self.game = game
        # Backing array for response and two features
        self.data = m = np.zeros((MAX_OBSERVATIONS, 3))
        # Responses
        self.velocity = m[:, 0]
        # Features
        self.prev_velocity = m[:, 1]
        self.prev_throttle = m[:, 2]
        # Counts
        self.n_total = 0
        self.n_since_fit = 0
        # Parameters
        self.beta_velocity = beta_velocity
        self.beta_throttle = beta_throttle
        # Other state
        self.has_been_fitted = False

    def collect_data(self):
        car = self.game.my_car
        s = car.state
        if car.is_active and car.has_complete_data and \
           not car.is_turbo_active and s.prev_throttle > 0.0 and \
           s.prev_velocity > 0.0 and s.velocity > 0.0:
            ix = self.n_total
            self.velocity[ix] = s.velocity
            self.prev_velocity[ix] = s.prev_velocity
            self.prev_throttle[ix] = s.prev_throttle
            self.n_total += 1
            self.n_since_fit += 1

    def max_velocity(self, boost=1.0):
        return boost * self.beta_throttle / (1.0 - self.beta_velocity)

    @property
    def should_fit(self):
        return self.n_total <= 100 and \
               self.n_since_fit >= 25

    @property
    def is_trained(self):
        return self.n_since_fit < self.n_total

    def fit(self):
        ix = np.s_[:self.n_total]
        y = self.velocity[ix]
        X = np.vstack((self.prev_velocity[ix], self.prev_throttle[ix])).T
        fit = la.lstsq(X, y)
        self.beta_velocity, self.beta_throttle = fit[0]
        self.n_since_fit = 0
        print 'INFO: beta_velocity = {:f}, beta_throttle = {:f}, N = {:d}' \
              .format(self.beta_velocity, self.beta_throttle, self.n_total)

    def calc_throttle(self, current_velocity, target_velocity, boost=1.0):
        t_raw = (target_velocity - self.beta_velocity * current_velocity) \
                / (boost * self.beta_throttle)
        if t_raw < 0.0:
            return 0.0
        elif t_raw > 1.0:
            return 1.0
        else:
            return t_raw

    def calc_velocities(self, init_velocity, throttle, boosts):
        v = float(init_velocity)
        beta_v = self.beta_velocity
        beta_t = self.beta_throttle
        velocities = np.zeros_like(boosts)
        #import pdb; pdb.set_trace()
        for i, b in enumerate(boosts):
            v = beta_v * v + beta_t * b * throttle
            velocities[i] = v
        distances = np.cumsum(velocities) / 60.0
        return velocities, distances

    def shortest_distance_to_stop(self, init_velocity):
        return float(init_velocity) / (60.0 * (1.0 - self.beta_velocity))

    def brake_velocities(self, init_velocity, brake, distance):
        if init_velocity == 0.0:
            return np.zeros(0), np.zeros(0)
        phi = self.beta_velocity ** brake
        if phi < 1.0:
            a = 1.0 - 60.0 * float(distance) / float(init_velocity) * (1.0 - phi)
            if a > 0.0:
                n = min(int(np.ceil(np.log(a) / np.log(phi))), 100)
            else:
                n = 100
            f = np.ones(n) * phi
            v = np.cumprod(f) * float(init_velocity)
        else:
            n = min(int(np.ceil(60.0 * float(distance) / float(init_velocity))), 100)
            v = np.ones(n) * float(init_velocity)
        d = np.cumsum(v) / 60.0
        return v, d

    def calc_throttle_for_braking(self, current_velocity, braking_factor,
                                  boost=1.0):
        target_velocity \
            = current_velocity * (self.beta_velocity ** braking_factor)
        throttle \
            = self.calc_throttle(current_velocity, target_velocity, boost)
        return throttle


class NonCentrifugalComponent(object):

    def __init__(self, game, beta_delta_angle=0.90,
                 beta_f_lateral_friction=0.001):
        self.game = game
        # Backing array
        self.data = m = np.zeros((MAX_OBSERVATIONS, 3))
        # Responses
        self.delta_angle = m[:, 0]
        # Features
        self.prev_delta_angle = m[:, 1]
        self.prev_f_lateral_friction = m[:, 2]
        # Counts
        self.n_total = 0
        self.n_since_fit = 0
        # Model parameters with conservative defaults
        self.beta_delta_angle = beta_delta_angle
        self.beta_f_lateral_friction = beta_f_lateral_friction

    def collect_data(self):
        car = self.game.my_car
        if car.is_active and car.has_complete_data:
            self.collect_car_data(car)

    def collect_car_data(self, car):
        s = car.state
        if s.prev_vanilla and s.vanilla and \
           s.prev_velocity > 0.0 and abs(s.prev_delta_angle) > 0.0 and \
           s.prev_curvature == 0.0:
            ix = self.n_total
            # Responses
            self.delta_angle[ix] = s.delta_angle
            # Features
            self.prev_delta_angle[ix] = s.prev_delta_angle
            self.prev_f_lateral_friction[ix] \
                = -np.sin(np.pi * s.prev_angle / 180.0) * s.prev_velocity
            self.n_total += 1
            self.n_since_fit += 1

    @property
    def should_fit(self):
        return self.n_since_fit >= 100

    def fit(self):
        ix = np.s_[:self.n_total]
        y = self.delta_angle[ix]
        X = np.vstack((self.prev_delta_angle[ix],
                       self.prev_f_lateral_friction[ix])).T
        fit = la.lstsq(X, y)
        self.beta_delta_angle = fit[0][0]
        self.beta_f_lateral_friction = fit[0][1]
        self.n_since_fit = 0
        print 'INFO: beta_delta_angle = {:f}, beta_f_lateral_friction = {:f}' \
              ', N = {:d}'.format(self.beta_delta_angle,
                                self.beta_f_lateral_friction, self.n_total)
        y_hat = np.dot(X, fit[0])


class CentrifugalComponent(object):

    def __init__(self, game, non_cf_model, beta_f_static_friction=0.85,
                 beta_f_centrifugal=0.001):
        self.game = game
        self.non_cf_model = non_cf_model
        # Backing array
        self.data = m = np.zeros((MAX_OBSERVATIONS, 5))
        # Responses
        self.delta_angle = m[:, 0]
        # Features
        self.prev_delta_angle = m[:, 1]
        self.prev_f_lateral_friction = m[:, 2]
        self.prev_f_centrifugal = m[:, 3]
        # Auxiliary data
        self.prev_curvature = m[:, 4]
        # Counts
        self.n_total = 0
        self.n_since_fit = 0
        self.dont_retry_in = 0
        # Model parameters with conservative defaults
        self.beta_f_static_friction = beta_f_static_friction
        self.beta_f_centrifugal = beta_f_centrifugal

    def collect_data(self):
        car = self.game.my_car
        if car.is_active and car.has_complete_data:
            self.collect_car_data(car)

    def collect_car_data(self, car):
        s = car.state
        if s.prev_vanilla and s.vanilla and \
           s.prev_velocity > 0.0 and abs(s.prev_delta_angle) > 0.0 and \
           abs(s.prev_curvature) > 0.0:
            ix = self.n_total
            # Responses
            self.delta_angle[ix] = s.delta_angle
            self.prev_delta_angle[ix] = s.prev_delta_angle
            self.prev_f_lateral_friction[ix] \
                = -np.sin(np.pi * s.prev_angle / 180.0) * s.prev_velocity
            self.prev_f_centrifugal[ix] \
                = np.abs(s.prev_curvature) * s.prev_velocity * s.prev_velocity
            self.prev_curvature[ix] = s.prev_curvature
            self.n_total += 1
            self.n_since_fit += 1
            self.dont_retry_in -= 1

    @property
    def should_fit(self):
        return self.n_since_fit > 100 and self.dont_retry_in < 1

    @property
    def has_been_fitted(self):
        return self.n_total > self.n_since_fit

    def fit(self):
        ix = np.s_[:self.n_total]
        # Remove the effect of the non-centrifugal components
        y_full = self.delta_angle[ix]
        X_friction \
            = np.vstack((self.prev_delta_angle[ix],
                         self.prev_f_lateral_friction[ix])).T
        beta_friction \
            = np.array((self.non_cf_model.beta_delta_angle,
                        self.non_cf_model.beta_f_lateral_friction))
        y_friction = np.dot(X_friction, beta_friction)
        y_residual = np.abs(y_full - y_friction)
        # Use only those observations that have clearly non-zero residual;
        # Effectively this selects those cases wherece ntrifugal force has
        # overcome the static friction.
        ix_valid = np.s_[y_residual > 0.1]
        # Fit linear model on non-zero residuals giving slope of the
        # centrifugal force (i.e. the mass) and static friction force
        # (i.e. the constant term).
        y = y_residual[ix_valid]
        n_valid = y.size
        if n_valid < 10:
            self.dont_retry_in = 25
            return
        X = np.vstack((np.ones(y.shape),
                       self.prev_f_centrifugal[ix][ix_valid])).T

        fit = la.lstsq(X, y)
        self.beta_f_static_friction = -fit[0][0]
        self.beta_f_centrifugal = fit[0][1]
        self.n_since_fit = 0
        print 'INFO: beta_f_static_friction = {:f}, beta_f_centrifugal = {:f}' \
              ', N = {:d}'.format(self.beta_f_static_friction,
                                  self.beta_f_centrifugal, n_valid)
        y_hat = np.dot(X, fit[0])


class DriftModel(object):

    def __init__(self, game,
                 beta_delta_angle=0.90,
                 beta_f_lateral_friction=0.001,
                 beta_f_static_friction=0.85,
                 beta_f_centrifugal=0.001):
        self.non_cf_model \
            = NonCentrifugalComponent(game,
                                      beta_delta_angle=beta_delta_angle,
                                      beta_f_lateral_friction=beta_f_lateral_friction)
        self.cf_model \
            = CentrifugalComponent(game, self.non_cf_model,
                                   beta_f_static_friction=beta_f_static_friction,
                                   beta_f_centrifugal=beta_f_centrifugal)
        self.have_told_is_trained = False
        self.is_trained = False

    # Hack!
    @property
    def is_just_trained(self):
        if self.is_trained and not self.have_told_is_trained:
            self.have_told_is_trained = True
            return True
        else:
            return False

    def collect_data(self):
        self.non_cf_model.collect_data()
        self.cf_model.collect_data()

    @property
    def should_fit(self):
        return self.non_cf_model.should_fit or \
               self.cf_model.should_fit

    def fit(self):
        if self.non_cf_model.should_fit:
            self.non_cf_model.fit()
            if self.cf_model.has_been_fitted:
                self.cf_model.fit()
                self.is_trained = True
        elif self.cf_model.should_fit:
            self.cf_model.fit()

    def safety_speed(self, curvatures):
        if np.isscalar(curvatures):
            curvatures = np.array((curvatures,))
        s = self.cf_model.beta_f_static_friction / \
            (curvatures * self.cf_model.beta_f_centrifugal)
        s[s < 10000.0] = 10000.0
        return np.sqrt(s)

    def limit_speed(self, curvatures, critical_angle):
        a = self.cf_model.beta_f_centrifugal * np.abs(curvatures)
        b = self.non_cf_model.beta_f_lateral_friction * np.sin(np.pi * critical_angle / 180.0)
        c = self.cf_model.beta_f_static_friction
        disc = b * b + 4.0 * a * c
        return 0.5 * (b + np.sqrt(disc)) / a

    def project_angle(self, init_angle, init_delta_angle, velocities, curvatures):
        # For convenience
        beta_da = self.non_cf_model.beta_delta_angle
        beta_f_lf = self.non_cf_model.beta_f_lateral_friction
        beta_f_cf = self.cf_model.beta_f_centrifugal
        beta_f_sf = self.cf_model.beta_f_static_friction
        # Compute the centrifugal component with ufuncs
        v = velocities
        c = np.array(curvatures)
        s = np.ones_like(c)
        s[c < 0.0] = -1.0
        c = np.abs(c)
        f = beta_f_cf * c * v * v - beta_f_sf
        f[f < 0.0] = 0.0
        if np.any(f < 0.0):
            import pdf
            pdb.set_trace()
        delta_angle_cf = s * f
        # Compute the rest of it slowly
        angle = init_angle
        delta_angle = init_delta_angle
        projected_angles = np.zeros_like(velocities)
        for i in xrange(v.size):
            delta_angle = beta_da * delta_angle \
                        - beta_f_lf * v[i] * np.sin(np.pi * angle / 180.0) \
                        + delta_angle_cf[i]
            angle += delta_angle
            projected_angles[i] = angle
        return projected_angles
