from collections import defaultdict
import const
import numpy as np
import random


class Action(object):

    NOOP = 0
    THROTTLE = 1
    SWITCH_LANE = 2
    TURBO_BOOST = 3

    LEFT = -1
    STRAIGHT = 0
    RIGHT = 1

    @classmethod
    def noop(cls):
        return cls(cls.NOOP)

    @classmethod
    def throttle(cls, amount):
        return cls(cls.THROTTLE, amount)

    @classmethod
    def switch_left(cls):
        return cls(cls.SWITCH_LANE, cls.LEFT)

    @classmethod
    def switch_right(cls):
        return cls(cls.SWITCH_LANE, cls.RIGHT)

    @classmethod
    def turbo_boost(cls):
        return cls(cls.TURBO_BOOST)

    def __init__(self, command=None, value=None):
        self.command = command
        self.value = value

    def as_api_message(self, tick=None):
        if self.command == self.NOOP:
            message = {'msgType': 'ping'}
        elif self.command == self.THROTTLE:
            message = {'msgType': 'throttle', 'data': self.value}
        elif self.command == self.SWITCH_LANE:
            message = {'msgType': 'switchLane'}
            if self.value == self.LEFT:
                message['data'] = 'Left'
            elif self.value == self.RIGHT:
                message['data'] = 'Right'
            else:
                assert False
        elif self.command == self.TURBO_BOOST:
            message = {'msgType': 'turbo', 'data': 'Long live the Queen!'}
        else:
            assert False
        if tick is not None:
            message['gameTick'] = tick
        return message


class MouseBrain(object):

    def __init__(self, game, critical_angle_model, throttle_model, drift_model):
        self.game = game
        self.critical_angle_model = critical_angle_model
        self.throttle_model = throttle_model
        self.drift_model = drift_model
        self.scary_level_step = 0.5 ** 0.25
        self.scary_level_orig = 0.65 / self.scary_level_step
        self.scary_level = self.scary_level_orig / self.scary_level_step
        # Known bug: probably wont work right (optimally) at the start of
        # subsequent races.
        self.last_checked_directions_on = None
        self.plan = []

    def project_angle(self, segment_ix, segment_dist, velocity, angle,
                      delta_angle, brake):
        horizon \
            = self.throttle_model.shortest_distance_to_stop(velocity)
        velos, dists \
            = self.throttle_model.brake_velocities(velocity, brake, horizon)
        curvs \
            = self.game.track.curvatures_from(segment_ix, segment_dist, dists)
        angles \
            = self.drift_model.project_angle(angle, delta_angle, velos, curvs)
        return angles

    def would_overshoot_with_braking(self, segment_ix, segment_dist, velocity,
                                     angle, delta_angle, brake):
        angles \
            = self.project_angle(segment_ix, segment_dist, velocity, angle,
                                 delta_angle, brake)
        weights = np.ones(angles.size) * 0.95
        weights = np.cumprod(weights)
        extreme_angle = np.max(np.abs(angles * weights))
        return extreme_angle > self.critical_angle_model.angle

    def throttle_ok(self, segment_ix, segment_dist, velocity, angle,
                    delta_angle, throttle):
        boosts = np.ones(20)
        velos, dists \
            = self.throttle_model.calc_velocities(velocity, throttle, boosts)
        curvs \
            = self.game.track.curvatures_from(segment_ix, segment_dist, dists)
        angles \
            = self.drift_model.project_angle(angle, delta_angle, velos, curvs)
        extreme_angle = np.max(np.abs(angles))
        #import pdb; pdb.set_trace()
        return extreme_angle < self.critical_angle_model.angle

    def calc_throttle(self, segment_ix, segment_dist, velocity, angle,
                     delta_angle):
        for t in np.linspace(1, 0, 10):
            max_t = t
            if self.throttle_ok(segment_ix, segment_dist, velocity, angle,
                                delta_angle, t):
                break
        return max_t

    def calc_braking(self, segment_ix, segment_dist, velocity, angle,
                      delta_angle):

        _f = lambda x: self.would_overshoot_with_braking(segment_ix,
                                                         segment_dist,
                                                         velocity,
                                                         angle,
                                                         delta_angle,
                                                         x)

        if not _f(0.0):
            return 0.0

        if _f(0.75):
            l, r = 0.75, 1.0
            for _ in xrange(6):
                m = 0.5 * (l + r)
                if _f(m):
                    l = m
                else:
                    r = m
            return r
        else:
            return 0.0

    def calc_safe_velo(self):

        s = self.game.my_car.state

        segment_ix = s.segment_ix
        in_segment_dist = s.in_segment_dist
        velocity = s.velocity

        tm = self.throttle_model
        to_stop = tm.shortest_distance_to_stop(velocity)
        if to_stop < 10.0:
            return self.drift_model.safety_speed(s.curvature)
        brake_velos, brake_dists = tm.brake_velocities(velocity, 1.0, to_stop * 0.5)
        factors = velocity / brake_velos
        curvatures \
            = self.game.track.curvatures_from(segment_ix,
                                              in_segment_dist,
                                              brake_dists,
                                              self.plan)
        safe_velos = self.drift_model.safety_speed(curvatures)
        adj_safe_velos = factors * safe_velos
        # import matplotlib.pyplot as pp
        # pp.figure()
        # pp.hold(True)
        # pp.plot(safe_velos)
        # pp.plot(adj_safe_velos)
        # import pdb; pdb.set_trace()
        safe_velo = np.min(adj_safe_velos)
        return safe_velo

    def calc_rad_velo(self):
        s = self.game.my_car.state

        segment_ix = s.segment_ix
        in_segment_dist = s.in_segment_dist
        velocity = s.velocity
        ca = self.critical_angle_model.angle

        tm = self.throttle_model
        to_stop = tm.shortest_distance_to_stop(velocity)
        if to_stop < 10.0:
            return self.drift_model.limit_speed(s.curvature, ca)
        brake_velos, brake_dists = tm.brake_velocities(velocity, 1.0, to_stop * 0.8)
        factors = velocity / brake_velos
        curvatures \
            = self.game.track.curvatures_from(segment_ix,
                                              in_segment_dist,
                                              brake_dists,
                                              self.plan)
        limit_velos = self.drift_model.limit_speed(curvatures, ca)
        adj_limit_velos = factors * limit_velos
        limit_velo = np.min(adj_limit_velos)
        return limit_velo

    def get_action(self):

        if not self.game.is_active:
            self.plan = []
            return Action.noop()

        my_car = self.game.my_car

        if not my_car.is_active:
            if my_car.crashed_on_previous_tick:
                if self.throttle_model.is_trained:
                    self.scary_level *= self.scary_level_step
                print 'INFO: scary drift level adjusted down to {:.3f}' \
                      .format(self.scary_level)
            return Action.noop()

        if not self.throttle_model.is_trained:
            return Action.throttle(0.25 + random.random() * 0.75)

        if not my_car.has_complete_data:
            return Action.throttle(1.0)

        # Bring it up a bit upon successful training
        if self.drift_model.is_just_trained:
            self.scary_level /= self.scary_level_step ** 2.0
            if self.scary_level > self.scary_level_orig:
                self.scary_level = self.scary_level_orig
            print 'INFO: scary drift level adjusted up to {:.3f}' \
                .format(self.scary_level)

        s = my_car.state
        segment_ix = s.segment_ix
        in_segment_dist = s.in_segment_dist
        velocity = s.velocity
        angle = s.angle
        delta_angle = s.delta_angle

        limit_velo = self.calc_rad_velo()

        safe_velo = self.calc_safe_velo()

        critical_angle = self.critical_angle_model.angle

        is_panic = False

        if not self.drift_model.is_trained:
            target_velo = limit_velo
        elif np.abs(angle) > self.scary_level * critical_angle:
            print 'angle = {:f}, delta_angle = {:f}' \
                  .format(angle, delta_angle)
            if (angle * delta_angle < 0.0):
                if np.abs(delta_angle) * (critical_angle - np.abs(angle)) > 5.0:
                    target_velo = limit_velo
                else:
                    is_panic = True
                    target_velo = safe_velo
            else:
                is_panic = True
                target_velo = safe_velo
        else:
            target_velo = limit_velo

        throttle = self.throttle_model.calc_throttle(velocity, target_velo,
                                                     my_car.boost_factor)

        if self.drift_model.is_trained and my_car.state.turbo_available and \
           target_velo > 2.0 * self.throttle_model.max_velocity():
            return Action.turbo_boost()

        if self.last_checked_directions_on != segment_ix and \
           not is_panic and s.prev_throttle is not None and \
           np.abs(throttle - s.prev_throttle) < 0.1:
            self.last_checked_directions_on = segment_ix
            direction = self.should_switch()
            if direction is not None:
                self.plan = [direction]
                if direction == const.LEFT:
                    print 'INFO: going to switch left'
                    return Action.switch_left()
                elif direction == const.RIGHT:
                    print 'INFO: going to switch right'
                    return Action.switch_right()
            else:
                self.plan = []

        if not self.drift_model.is_trained:
            if angle == 0.0:
                throttle *= 3.0
            if np.abs(angle) < 0.5 * self.scary_level * critical_angle:
                throttle *= 1.0 + 2.0 * random.random()
            elif np.abs(angle) < 0.75 * self.scary_level * critical_angle:
                throttle *= 1.0 + 0.5 * random.random()
            elif np.abs(angle) < self.scary_level * critical_angle:
                throttle *= 1.0 + 0.25 * random.random()
            if throttle > 1.0:
                throttle = 1.0

        # Guard again nan bug
        if np.isnan(throttle):
            throttle = 0.5

        return Action.throttle(throttle)

    def should_switch(self):

        game = self.game
        track = self.game.track
        me = self.game.my_car

        my_segment_ix = me.state.segment_ix
        my_in_segment_dist = me.state.in_segment_dist
        my_segment = track.segments[my_segment_ix]
        my_velocity = me.state.velocity or 0.0
        # We cannot plan for current segment as it is already too late
        upcoming_segment = my_segment.straight
        # If we cannot switch there is no point bothering about it
        if not upcoming_segment.is_branch_point:
            return None
        # See where the opponents are
        radar = defaultdict(list)
        for opponent in game.iter_opponents():
            her_segment_ix = opponent.state.segment_ix
            her_in_segment_dist = opponent.state.in_segment_dist
            her_velocity = opponent.state.velocity or 0.0
            radar[her_segment_ix].append((her_in_segment_dist,
                                          her_velocity))
        # Worst case: we are tailing someone; switch at random so that we
        # don't accidently end up doing deterministically the same moves with
        # the opponent.
        if my_segment_ix in radar:
            for her_in_segment_dist, _ in radar[my_segment_ix]:
                if her_in_segment_dist > my_in_segment_dist:
                    chosen_option \
                        = random.choice(upcoming_segment.list_options())
                    return chosen_option[0]
        # Calculate alternatives without thinking about opponents
        alternatives = []
        for direction, first_segment in upcoming_segment.list_options():
            length = 0.0
            # NB: Inf is an sentinel
            blockages = [float('inf')]
            for segment in first_segment.iter_till_branch_point():
                if segment.ix in radar:
                    for her_segment_ndist, her_velocity in sorted(radar[segment.ix]):
                        her_dist = length + her_in_segment_dist + \
                                   upcoming_segment.length + my_segment.length - \
                                   my_in_segment_dist
                        my_relative_velocity = my_velocity - her_velocity
                        if my_relative_velocity > 0.0:
                            time_to_reach = her_dist / my_relative_velocity
                        else:
                            time_to_reach = float('inf')
                        blockages.append(time_to_reach)
                length += segment.length
            blockages.sort()
            # Lower better
            score = 1.0 / blockages[0]
            alternatives.append((score, length, direction))
        alternatives.sort()
        #
        chosen_direction = alternatives[0][2]
        #
        return chosen_direction
