import inspect
import sys
import threading


def method_not_implemented(obj):
    print >> sys.stderr, 'WARNING: {:s} not implemented' \
        .format(obj.__class__.__name__ + '.' + inspect.stack()[1][3])


class RunningCondition(object):

    def __init__(self):
        self.flag = threading.Event()

    @property
    def can_run(self):
        return not self.flag.is_set()

    def fail(self):
        self.flag.set()
