import sys

from roypec.brain import Action
from roypec.util import method_not_implemented


class Handler(object):

    def __init__(self):
        self.dispatch_table = {
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'dnf': self.on_dnf,
            'error': self.on_error,
            'finish': self.on_finish,
            'gameEnd': self.on_game_end,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'join': self.on_join,
            'joinRace': self.on_join_race,
            'lapFinished': self.on_lap_finished,
            'spawn': self.on_spawn,
            'tournamentEnd': self.on_tournament_end,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'yourCar': self.on_your_car,
        }
        self.tick = None

    def handle(self, message, respond_fn):
        msg_type = message['msgType']
        data = message['data']
        tick = message.get('gameTick')
        if tick is not None and tick != self.tick:
            if self.tick is not None:
                self.on_tick_end()
            self.tick = tick
            self.on_tick_start(tick)
        handler_fn = self.dispatch_table[msg_type]
        response = handler_fn(data)
        if response:
            respond_fn(response)

    def on_dnf(self, tick):
        method_not_implemented(self)

    def on_tick_start(self, tick):
        method_not_implemented(self)

    def on_tick_end(self):
        method_not_implemented(self)

    def on_car_positions(self, data):
        method_not_implemented(self)

    def on_crash(self, data):
        method_not_implemented(self)

    def on_error(self, data):
        method_not_implemented(self)

    def on_finish(self, data):
        method_not_implemented(self)

    def on_game_end(self, data):
        method_not_implemented(self)

    def on_game_init(self, data):
        method_not_implemented(self)

    def on_game_start(self, data):
        method_not_implemented(self)

    def on_join(self, data):
        method_not_implemented(self)

    def on_join_race(self, data):
        method_not_implemented(self)

    def on_lap_finished(self, data):
        method_not_implemented(self)

    def on_spawn(self, data):
        method_not_implemented(self)

    def on_tournament_end(self, data):
        method_not_implemented(self)

    def on_turbo_available(self, data):
        method_not_implemented(self)

    def on_turbo_start(self, data):
        method_not_implemented(self)

    def on_your_car(self, data):
        method_not_implemented(self)


class PingPongHandler(Handler):

    def on_car_positions(self, data):
        return {'msgType': 'ping', 'data': 'on_car_positions'}

    def on_crash(self, data):
        return {'msgType': 'ping', 'data': 'on_crash'}

    def on_error(self, data):
        return {'msgType': 'ping', 'data': 'on_error'}

    def on_finish(self, data):
        return {'msgType': 'ping', 'data': 'on_finish'}

    def on_game_end(self, data):
        return {'msgType': 'ping', 'data': 'on_game_end'}

    def on_game_init(self, data):
        return {'msgType': 'ping', 'data': 'on_game_init'}

    def on_game_start(self, data):
        return {'msgType': 'ping', 'data': 'on_game_start'}

    def on_join(self, data):
        return {'msgType': 'ping', 'data': 'on_join'}

    def on_join_race(self, data):
        return {'msgType': 'ping', 'data': 'on_join_race'}

    def on_lap_finished(self, data):
        return {'msgType': 'ping', 'data': 'on_lap_finished'}

    def on_spawn(self, data):
        return {'msgType': 'ping', 'data': 'on_spawn'}

    def on_tournament_end(self, data):
        return {'msgType': 'ping', 'data': 'on_tournament_end'}

    def on_turbo_available(self, data):
        return {'msgType': 'ping', 'data': 'on_turbo_available'}

    def on_turbo_start(self, data):
        return {'msgType': 'ping', 'data': 'on_turbo_start'}

    def on_your_car(self, data):
        return {'msgType': 'ping', 'data': 'on_your_car'}


class GameHandler(Handler):

    def __init__(self, game, brain, observers=[]):
        super(GameHandler, self).__init__()
        self.game = game
        self.brain = brain
        self.observers = observers

    def on_tick_start(self, tick):
        self.game.advance_state(tick)

    def on_tick_end(self):
        for observer in self.observers:
            observer.collect_data()
            if observer.should_fit:
                observer.fit()

    def on_car_positions(self, api_data):
        for r in api_data:
            car_id = r['id']['color']
            p = r['piecePosition']
            piece_ix = int(p['pieceIndex'])
            l = p['lane']
            in_lane = int(l['startLaneIndex'])
            out_lane = int(l['endLaneIndex'])
            in_piece_dist = float(p['inPieceDistance'])
            angle = float(r['angle'])
            segment_ix = piece_ix, in_lane, out_lane,
            car = self.game.cars[car_id]
            car.note_position(self.tick, segment_ix, in_piece_dist, angle)
        action = self.brain.get_action()
        self.post_handle_action(action)
        return action.as_api_message(self.tick)

    def on_crash(self, data):
        car = data['color']
        self.game.note_crash(self.tick, car)

    def on_game_init(self, data):
        self.game.init_from_desc(data)

    def on_game_start(self, data):
        self.game.start(self.tick)
        action = self.brain.get_action()
        self.post_handle_action(action)
        return action.as_api_message(self.tick)

    def on_game_end(self, end):
        self.game.end(self.tick)

    def on_lap_finished(self, data):
        car = data['car']['color']
        lap = int(data['lapTime']['lap'])
        self.game.note_lap_finished(self.tick, car, lap)

    def on_spawn(self, data):
        car = data['color']
        self.game.note_spawn(self.tick, car)

    def on_tournament_one(self, data):
        self.game.end_tournament()

    def on_turbo_available(self, data):
        factor = data['turboFactor']
        duration_msecs = data['turboDurationMilliseconds']
        self.game.note_turbo_availability(factor, duration_msecs)

    def on_turbo_start(self, data):
        car = data['color']
        self.game.note_turbo_start(self.tick, car)

    def on_turbo_end(self, data):
        car = data['color']
        self.game.note_turbo_end(self.tick, car)

    def on_your_car(self, data):
        car = data['color']
        self.game.set_own_car(car)

    def post_handle_action(self, action):
        if action.command == Action.THROTTLE:
            self.game.my_car.set_throttle(action.value)
        elif action.command == Action.SWITCH_LANE:
            self.game.my_car.set_pending_switch(action.value)
        elif action.command == Action.TURBO_BOOST:
            # TODO: Here I'm relying on the server to respond with the turbo
            # start message before next action.
            pass

    def on_error(self, data):
        print >> sys.stderr, 'ERROR: {:s}'.format(data)

    def on_dnf(self, data):
        print >> sys.stderr, 'ERROR: {:s}'.format(data)
